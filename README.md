# Many people like and use minimalist Linux distros like Gentoo, Void or Parabola, but those distros needs internet connection to be installed. Of course, you can plug-in ethernet cabble, install it, install nm and done, but you can do it other way

## dhcpcd & wpa_supplicant
To get working unbloated WiFi, you will need two programs: dhcpcd and wpa_supplicant.

### First
First, fire up dhcpcd with 'rc-service dhcpcd start' in OpenRC or type as root 'dhcpcd &'.

### Next
Next, check your WiFi devices name with 'ifconfig -a' command. If ifconfig isn't available, use 'ip a' - this will do the same thing.

### Editing wpa_supplicant.conf
wpa_supplicant isnt like NetworkManager, where you're graphically selecting network. To get wpa_supplicant running, you have to edit /etc/wpa_supplicant.conf file.

### What to type?
Type this command, replace wifiname with network name, and replace psk000 with network password:

    rm -rf /etc/wpa_supplicant.conf
    cat >> /etc/wpa_supplicant.conf << "EOF"
    ctrl_interface=/var/run/wpa_supplicant

    network={
        ssid="wifiname"
        psk="psk000"
    }
    EOF

When the file is edited, start the wpa_supplicant with

    wpa_supplicant -iwlp2s0 -c/etc/wpa_supplicant.conf &

command, replace wlp2s0 with your WiFi device, and press enter. Enjoy!